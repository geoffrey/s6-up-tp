.equ PINA = 0x00 ; définition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D
.equ SREG = 0x3F

.equ ADMUX = 0x7C

.equ ADCSRB = 0x7B
.equ ADCSRA = 0x7A
.equ ADCH = 0x79
.equ ADCL = 0x78

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x003A
	jmp irqadc

.org 0x0080

codeAff:

.db 0b1111110, 0b001100
.db 0b0110111, 0b0011111
.db 0b1001101, 0b1011011
.db 0b1111011, 0b0001110
.db 0b1111111, 0b1011111

debut:
	ldi r28, low(RAMEND)
	ldi r29, high(RAMEND)

	out SPL, r28
	out SPH, r29

    ; 	DDRA@IO <- 0xFF
    LDI R16,0xFF
    OUT DDRA,R16

    ; 	ADMUX <- 0b01100000
    LDI R16,0b01100000
    STS ADMUX,R16

    ; 	ADCSRB <- 0b00000000
    LDI R16,0b00000000
    STS ADCSRB,R16

    ; 	ADCSRA <- 0b11101111
    LDI R16,0b11101111
    STS ADCSRA,R16


	sei

boucle:
	sleep
	jmp boucle

irqadc:
    ; 	PORTA@IO <- codeAff@ROM[ADCH/27]
    LDS R16,ADCH
    LDI R17,27
    SER R18
eti0:
    INC R18
    SUB R16,R17
    BRCC eti0
    MOV R16,R18
    LDI R30,low(codeAff<<1)
    LDI R31,high(codeAff<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    OUT PORTA,R16

	reti






