.equ PINA = 0x00 ; définition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02
.equ PINB = 0x03
.equ DDRB = 0x04
.equ PORTB = 0x05

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D
.equ SREG = 0x3F

.equ ADMUX = 0x7C
.equ ADCSRB = 0x7B
.equ ADCSRA = 0x7A
.equ ADCH = 0x79
.equ ADCL = 0x78

.def temp = r20
.def consigne = r21


.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x0080

codeAff:

.db 0b1111110, 0b001100
.db 0b0110111, 0b0011111
.db 0b1001101, 0b1011011
.db 0b1111011, 0b0001110
.db 0b1111111, 0b1011111

debut:
	ldi r28, low(RAMEND)
	ldi r29, high(RAMEND)

	out SPL, r28
	out SPH, r29

    ; 	DDRA@IO <- 0b01111111
    LDI R16,0b01111111
    OUT DDRA,R16

    ; 	DDRB@IO <- 0xFF
    LDI R16,0xFF
    OUT DDRB,R16


    ; 	ADCSRB <- 0b01000010
    LDI R16,0b01000010
    STS ADCSRB,R16





boucle:

    ; Déclenchement de le lecture de la consigne
    ; 	ADMUX <- 0b01100000
    LDI R16,0b01100000
    STS ADMUX,R16

    ; 	ADCSRA <- 0b11100111
    LDI R16,0b11100111
    STS ADCSRA,R16


attente1:
    ; 	si (ADCSRA & 0b00010000) = 0 saut attente1
    LDS R16,ADCSRA
    ANDI R16,0b00010000
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti0
    CLR R16
    RJMP eti1
eti0:
    LDI R16,0x01
eti1:
    TST R16
    BREQ eti2
    JMP attente1
eti2:


    ; Lecture de la consigne
    ; 	consigne <- ADCH / 8 + 20
    LDS R16,ADCH
    LDI R17,8
    SER R18
eti3:
    INC R18
    SUB R16,R17
    BRCC eti3
    MOV R16,R18
    LDI R17,20
    ADD R16,R17
    STS consigne,R16


    ; Mode hors-gel
    ; 	si (PINA@IO & 0b10000000) == 0 alors consigne <- 12
    IN R16,PINA
    ANDI R16,0b10000000
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti4
    CLR R16
    RJMP eti5
eti4:
    LDI R16,0x01
eti5:
    TST R16
    BREQ eti6
    LDI R16,12
    STS consigne,R16
eti6:


    ; Déclenchement de le lecture de la température
    ; 	ADMUX <- 0b01100001
    LDI R16,0b01100001
    STS ADMUX,R16

    ; 	ADCSRA <- 0b11100111
    LDI R16,0b11100111
    STS ADCSRA,R16


attente2:
    ; 	si (ADCSRA & 0b00010000) = 0 saut attente2
    LDS R16,ADCSRA
    ANDI R16,0b00010000
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti7
    CLR R16
    RJMP eti8
eti7:
    LDI R16,0x01
eti8:
    TST R16
    BREQ eti9
    JMP attente2
eti9:


    ; 	temp <- ADCH / 4
    LDS R16,ADCH
    LDI R17,4
    SER R18
eti10:
    INC R18
    SUB R16,R17
    BRCC eti10
    MOV R16,R18
    STS temp,R16


    ; Affichage
    ; 	PORTA@IO <- codeAff@ROM[temp/20]
    LDS R16,temp
    LDI R17,20
    SER R18
eti11:
    INC R18
    SUB R16,R17
    BRCC eti11
    MOV R16,R18
    LDI R30,low(codeAff<<1)
    LDI R31,high(codeAff<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    OUT PORTA,R16

    ; 	PORTB@IO <- codeAff@ROM[temp-(temp/20)*20]
    LDS R16,temp
    PUSH R16
    LDS R16,temp
    LDI R17,20
    SER R18
eti12:
    INC R18
    SUB R16,R17
    BRCC eti12
    MOV R16,R18
    LDI R17,20
    MUL R16,R17
    MOV R16,R0
    MOV R17,R16
    POP R16
    SUB R16,R17
    LDI R30,low(codeAff<<1)
    LDI R31,high(codeAff<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    OUT PORTB,R16


    ; Gestion du radiateur
    ; 	si consigne - 1 > temp alors PORTB@IO <- PORTB@IO | 0b10000000
    LDS R16,consigne
    LDI R17,1
    SUB R16,R17
    PUSH R16
    LDS R16,temp
    POP R17
    CP R17,R16
    BREQ eti13
    BRLO eti13
    LDI R16,0x01
    RJMP eti14
eti13:
    CLR R16
eti14:
    TST R16
    BREQ eti15
    IN R16,PORTB
    ORI R16,0b10000000
    OUT PORTB,R16
eti15:

    ; 	si consigne + 1 < temp alors PORTB@IO <- PORTB@IO & 0b01111111
    LDS R16,consigne
    LDI R17,1
    ADD R16,R17
    PUSH R16
    LDS R16,temp
    POP R17
    CP R17,R16
    BRLO eti16
    CLR R16
    RJMP eti17
eti16:
    LDI R16,0x01
eti17:
    TST R16
    BREQ eti18
    IN R16,PORTB
    ANDI R16,0b01111111
    OUT PORTB,R16
eti18:


	jmp boucle


