.equ PINA = 0x00 ; définition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02
.equ PINC = 0x06
.equ DDRC = 0x07
.equ PORTC = 0x08

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D
.equ SREG = 0x3F

.equ TCCR0A = 0x24
.equ TCCR0B = 0x25
.equ TIMSK0 = 0x6E
.equ TIFR0 = 0x35

.equ ADMUX = 0x7C
.equ ADCSRB = 0x7B
.equ ADCSRA = 0x7A
.equ ADCH = 0x79
.equ ADCL = 0x78

.def d2 = r19
.def d1 = r20
.def d0 = r21
.def select = r22
.def temp = r23

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x002E ; Interruption du timer
	jmp tm

.org 0x003A
	jmp irqadc

.org 0x0080

codeAff:
	.db 0b1111110, 0b001100
	.db 0b0110111, 0b0011111
	.db 0b1001101, 0b1011011
	.db 0b1111011, 0b0001110
	.db 0b1111111, 0b1011111

debut:
	ldi r28, low(RAMEND)
	ldi r29, high(RAMEND)

	out SPL, r28
	out SPH, r29

	cli
    ; 	DDRA@IO <- 0xFF
    LDI R16,0xFF
    OUT DDRA,R16

    ; 	DDRC@IO <- 0xFF
    LDI R16,0xFF
    OUT DDRC,R16


    ; 	ADMUX <- 0b01100001
    LDI R16,0b01100001
    STS ADMUX,R16

    ; 	ADCSRB <- 0b00000000
    LDI R16,0b00000000
    STS ADCSRB,R16

    ; 	ADCSRA <- 0b11101111
    LDI R16,0b11101111
    STS ADCSRA,R16


	; Timer toutes les 2 ms
    ; 	TCCR0A@IO <- 0x00
    LDI R16,0x00
    OUT TCCR0A,R16

    ; 	TCCR0B@IO <- 0x04
    LDI R16,0x04
    OUT TCCR0B,R16

    ; 	TIMSK0 <- 0x01
    LDI R16,0x01
    STS TIMSK0,R16

    ; 	TIFR0 <- 0x01
    LDI R16,0x01
    STS TIFR0,R16


	sei

    ; 	d2 <- 1
    LDI R16,1
    STS d2,R16

    ; 	d1 <- 2
    LDI R16,2
    STS d1,R16

    ; 	d0 <- 3
    LDI R16,3
    STS d0,R16

    ; 	select <- 0b00100000
    LDI R16,0b00100000
    STS select,R16


boucle:
	sleep
	jmp boucle


irqadc:
    ; 	temp <- ADCH
    LDS R16,ADCH
    STS temp,R16

    ; 	d2 <- temp / 100
    LDS R16,temp
    LDI R17,100
    SER R18
eti0:
    INC R18
    SUB R16,R17
    BRCC eti0
    MOV R16,R18
    STS d2,R16

    ; 	d1 <- temp / 10 - (temp / 100) * 10
    LDS R16,temp
    LDI R17,10
    SER R18
eti1:
    INC R18
    SUB R16,R17
    BRCC eti1
    MOV R16,R18
    PUSH R16
    LDS R16,temp
    LDI R17,100
    SER R18
eti2:
    INC R18
    SUB R16,R17
    BRCC eti2
    MOV R16,R18
    LDI R17,10
    MUL R16,R17
    MOV R16,R0
    MOV R17,R16
    POP R16
    SUB R16,R17
    STS d1,R16

    ; 	d0 <- temp - temp/10*10
    LDS R16,temp
    PUSH R16
    LDS R16,temp
    LDI R17,10
    SER R18
eti3:
    INC R18
    SUB R16,R17
    BRCC eti3
    MOV R16,R18
    LDI R17,10
    MUL R16,R17
    MOV R16,R0
    MOV R17,R16
    POP R16
    SUB R16,R17
    STS d0,R16

	reti

tm:
    ; 	PortC@IO <- select
    LDS R16,select
    OUT PortC,R16

    ; 	si select = 0b10000000 alors PortA@IO <- codeAff@ROM[d2]
    LDS R16,select
    PUSH R16
    LDI R16,0b10000000
    POP R17
    CP R17,R16
    BREQ eti4
    CLR R16
    RJMP eti5
eti4:
    LDI R16,0x01
eti5:
    TST R16
    BREQ eti6
    LDS R16,d2
    LDI R30,low(codeAff<<1)
    LDI R31,high(codeAff<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    OUT PortA,R16
eti6:

    ; 	si select = 0b01000000 alors PortA@IO <- codeAff@ROM[d1]
    LDS R16,select
    PUSH R16
    LDI R16,0b01000000
    POP R17
    CP R17,R16
    BREQ eti7
    CLR R16
    RJMP eti8
eti7:
    LDI R16,0x01
eti8:
    TST R16
    BREQ eti9
    LDS R16,d1
    LDI R30,low(codeAff<<1)
    LDI R31,high(codeAff<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    OUT PortA,R16
eti9:

    ; 	si select = 0b00100000 alors PortA@IO <- codeAff@ROM[d0]
    LDS R16,select
    PUSH R16
    LDI R16,0b00100000
    POP R17
    CP R17,R16
    BREQ eti10
    CLR R16
    RJMP eti11
eti10:
    LDI R16,0x01
eti11:
    TST R16
    BREQ eti12
    LDS R16,d0
    LDI R30,low(codeAff<<1)
    LDI R31,high(codeAff<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    OUT PortA,R16
eti12:

	lsl select
    ; 	si select = 0 alors select <- 0b00100000
    LDS R16,select
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti13
    CLR R16
    RJMP eti14
eti13:
    LDI R16,0x01
eti14:
    TST R16
    BREQ eti15
    LDI R16,0b00100000
    STS select,R16
eti15:

	reti


