# Compte-rendu TP3 Microprocesseurs
## REVIRON Arthur PREUD'HOMME Geoffrey (IMA3 TP2)

Avec ce TP nous allons pouvoir nous familiariser avec le convertisseur analogique-numérique (ou ADC).

**Note :** Bien que le code des programmes réalisés soient inclus dans ce compte-rendu, ils ne servent qu'à donner une vue d'ensemble et à justifier le travail réalisé.

# Prise en main de l'interface ADC (TP30)

Les deux programmes permettent d'afficher en continu les valeurs entrées sur ADC0 sur l'afficheur 7-segments situé PortA. Les conversions sont déclenchées à la suite (free running mode). Le premier programme utilise cependant une scrutation et ne se préoccupe pas de savoir si la conversion est terminée. Le deuxième programme déclenche une interruption pour chaque conversion terminée, et l'afficheur n'est mis à jour qu'à ce moment là, ce qui évite de gaspiller des ressources inutilement et évite aussi de récupérer des valeurs fausses dûes à une conversion pas totalement terminée (ce qui dans ce cas ne change quelque chose que pour la première scrutation).

!include(TP30b.txt lang=avrasmplus)

# Capteur de température (TP31)

Il est ici question d'utiliser un convertisseur analogique-numérique pour récupérer les valeurs d'une sonde de température pour l'afficheur dans un résultat plus lisible.

On réutilisera la même configuration que pour le deuxième programme du TP30, cependant l'affichage va varier un peu. En effet, on veut transformer la valeur reçue de l'ADC sur [0, 255] en valeur sur [0, 32]. Pour cela, on divise d'abord la valeur par $\frac{255}{32}\approx 8$. Puis on affiche les dizaines sur le premier afficheur et les unités sur le deuxième. D'un point de vue mathématique, on aurait aussi pu multiplier par $32$ puis diviser par $255$ ou multiplier par $\frac{32}{255}=0,12$, cependant ce n'est pas possible sur ce micro-contrôleur (du moins simplement) car les entiers sont stockés sur 8 bits donc leur valeur ne peuvent dépasser $255$, et les nombres à virgule ne sont pas supportés.

!include(TP31.txt lang=avrasmplus)

# Capteur de distance (TP32)

Il est demandé d'afficher la tension fournie par le capteur infrarouge situé sur la maquette. Ne connaissant pas au préalable l'équivalence entre la tension fournie et la distance en cm, on affichera la valeur de la tension sur le plus grand intervalle possible qui est de [0, 255]. On aurait en réalité pu utiliser les 10 bits de précision que nous fournit l'ADC, mais en voyant qu'avec 8 on on obtient beaucoup de bruit nous n'avons pas jugé cela nécessaire.

On réutilise donc le code du TP22 nous permettant d'afficher un nombre différent sur les 3 afficheurs 7-segments disponible sur la carte. Pour rappel, cet afficheur utilise 4 registres, nommé `d2`, `d1`, `d0` et `select`. Les trois premiers correspondent au chiffre à afficher pour chacun des 3 afficheurs. `select` contient quant à lui l'état du port C nécessaire pour afficher un des trois digits. On configure le timer pour qu'il effectue une interruption toutes les 2 ms, dans laquelle il décale `select` afin de sélectionner un autre afficheur et envoie la représentation du digit correspondant sur le port A.

!include(TP32.txt lang=avrasmplus)

![Visualisation du capteur de distance](capteur_distance_vect.svg)

Le capteur semble décroitre exponentiellement au fur et à mesure qu'on éloigne un obstacle de devant lui. Cependant, on remarque que si l'obstacle se rapproche trop, le capteur considère qu'il s'éloigne. Il a donc un intervalle de fonctionnement assez faible.

# Régulateur pour chauffage central (TP33)

Nous allons réaliser une application concrète de l'utilisation de l'ADC en construisant un régulateur pour chauffage central. La difficulté sera ici de lire deux entrées d'ADC en alternance : il n'est donc pas possible d'utiliser le free-running mode que nous avons utilisé jusqu'ici. Le micro-contrôleur nous propose nativement de pouvoir comparer électriquement deux entrées d'ADC pour n'avoir à traiter qu'un seul résultat, cependant nous ne pourrons pas l'utiliser dans ce cas là car les deux entrées analogiques ne sont pas sur la même échelle (la première est sur [10, 26]°C, la deuxième est sur [0, 32]°C).

On utilisera donc aucun mode particulier, ni d'interruption, et on activera le bit permettant de déclencher une conversion sur `ADCSRA` après avoir sélectionné l'entre ADC que l'on désire avec `ADCMUX`, puis on scrutera le bit de réception pour continuer la suite du programme quand la conversion a été faite et on peut récupérer la valeur. Pour réduire le temps d'éxecution du programme (sa réactivité ici, étant donné que le programme tourne en boucle), on pourrait lancer une conversion dès que la précédente est terminée et qu'on a récupéré la valeur d'`ACDH` et de réaliser le traitement de la valeur précédente en même temps que la suivante est en train d'être convertie, plutôt que de passer en mode scrutation directement après le lancement de la conversion.

Un problème que nous avons rencontré est que l'on donnait une valeur à `ADMUX` deux fois avant que la conversion soit terminée (une fois lors de l'initialisation du programme et une autre fois dans la boucle). Cela avait pour effet de provoquer un comportement instable au niveau des conversions sur toute la durée d'éxecution du programme (et non que pour les premières lectures). Comme quoi vouloir s'assurer qu'un composant est bien configuré peut entraîner l'effet inverse.

Le programme alterne entre la lecture de la consigne et la lecture de la température réelle, qu'il stocke dans les registres `consigne` et `temperature`, respectivement. Pour gérer le mode hors-gel, `consigne` est écrasé avec la température du mode hors-gel si l'entrée correspondante (`PA7`) est active. Il aurait été plus judicieux de vérifier à priori si le mode hors-gel est actif et ainsi éviter une conversion inutile. Une fois les deux valeurs récupérées, on détermine si il faut allumer le chauffage ou l'éteindre, sinon le laisser dans son état actuel.

!include(TP33.txt lang=avrasmplus)

On remarque que la régulation se fait assez rapidement, et le chauffage "clignote" assez vite malgré l'hystéresis voulue. Dans un modèle plus fidèle à la réalité, où la température ne varie pas aussi rapidement, on préfèrerait mettre la procédure de mise à jour de l'état du chauffage (et donc par extensiion de la lecture des valeurs de consigne et d'entrée) dans une interruption watchdog à intervalle de temps assez grand (de l'ordre de plusieurs secondes) pour éviter d'utiliser des ressources inutilement, et éviter d'allumer et éteindre le chauffage trop fréquement malgré le phénomène d'hystéresis.


