﻿.equ PINA = 0x00 ; définition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02
.equ PINB = 0x03
.equ DDRB = 0x04
.equ PORTB = 0x05

.equ RAMEND = 0x21FF
.equ SPH = 0x3E
.equ SPL = 0x3D

.equ SREG = 0x3F

.equ ADMUX = 0x7C
.equ ADCSRB = 0x7B
.equ ADCSRA = 0x7A
.equ ADCH = 0x79
.equ ADCL = 0x78

.def temp = r20

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x003A
	jmp irqadc

.org 0x0080

codeAff: ; Représentation des chiffres sur l'afficheur 7-segments
    .db 0b1111110, 0b001100, 0b0110111, 0b0011111, 0b1001101, 0b1011011, 0b1111011, 0b0001110, 0b1111111, 0b1011111

debut:
	ldi r28, low(RAMEND)
	ldi r29, high(RAMEND)
	
	out SPL, r28
	out SPH, r29
	
	DDRA@IO <- 0xFF ; Port A (afficheur 7-segments) en sortie
	DDRB@IO <- 0xFF ; Port B (afficheur 7-segments) en sortie
	ADMUX <- 0b01100000 ; Sélection de l'ADC0
	ADCSRB <- 0b00000000 ; Free running mode
	ADCSRA <- 0b11101111 ; Activation des interruptions
	
	sei
	
boucle:
	sleep
	jmp boucle
	
irqadc:
	temp <- ADCH / 8
	PORTA@IO <- codeAff@ROM[temp/10]
	PORTB@IO <- codeAff@ROM[temp-(temp/10)*10] ; temp%10
	reti
	
	



