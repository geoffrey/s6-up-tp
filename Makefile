.PHONY: default cleantmp clean

default: $(subst md,pdf,$(wildcard *.md))

SOURCES=$(wildcard *.asm) $(wildcard *.txt)

%.pdf: %.html
	html2pdf -i "$<" -o "$@" -t "$* - DJERABA Taky PREUD'HOMME Geoffrey"

%.html: %.tmp ../template.html
	md2html -i "$<" -o "$@" -t ../template.html

%.tmp: %.md $(SOURCES)
	markedpp "$<" > "$@"

cleantmp:
	rm -rf $(subst md,html,$(wildcard *.md)) *.tmp

clean: cleantmp
	rm -rf $(subst md,pdf,$(wildcard *.md))
