.equ PINA = 0x00 ; définition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02
.equ PINB = 0x03
.equ DDRB = 0x04
.equ PORTB = 0x05
.equ PIND = 0x09
.equ DDRD = 0x0A
.equ PORTD = 0x0B

.equ SREG = 0x3F

.equ WDTCSR = 0x60
.equ TCCR0A = 0x24
.equ TCCR0B = 0x25
.equ TIMSK0 = 0x6E
.equ TIFR0 = 0x35

.equ EIMSK = 0x3D
.equ EICRA = 0x69
.equ EICRB = 0x6A

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D

.def d1 = r20
.def d0 = r21
.def route = r22

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x0002
	jmp b10s

.org 0x0004
	jmp b1s

.org 0x0006
	jmp marche

.org 0x0008
	jmp arret

.org 0x0018
	jmp wd

.org 0x0080

afficheur:
    .DB 0x7E, 0x0C, 0x37, 0x9F, 0x4D, 0xDB, 0xFB, 0x0E, 0xFF, 0xDF
    ;      0,    1,    2,    3,    4,    5,    6,    7,    8,    9

debut:
    ; 	d1 <- 0
    LDI R16,0
    STS d1,R16

    ; 	d0 <- 0
    LDI R16,0
    STS d0,R16

    ; 	route <- 0
    LDI R16,0
    STS route,R16


	cli
    ; 	SREG <- 0b10000000
    LDI R16,0b10000000
    STS SREG,R16


	; Configuration des ports
    ; 	DDRA@IO <- 0xFF
    LDI R16,0xFF
    OUT DDRA,R16

    ; 	DDRB@IO <- 0xFF
    LDI R16,0xFF
    OUT DDRB,R16

    ; 	DDRD@IO <- 0x80
    LDI R16,0x80
    OUT DDRD,R16


	; Interruptions
    ; 	EIMSK <- 0b00001111 ; On active les interruptions PD[3:0] sur front descendant
    LDI R16,0b00001111
    STS EIMSK,R16

    ;     EICRA <- 0b10101010
    LDI R16,0b10101010
    STS EICRA,R16

    ;     EICRB <- 0b00000000
    LDI R16,0b00000000
    STS EICRB,R16


	; Watchdog toutes les secondes
    ; 	WDTCSR <- 0x10
    LDI R16,0x10
    STS WDTCSR,R16

    ; 	WDTCSR <- 0b01000110
    LDI R16,0b01000110
    STS WDTCSR,R16


	sei

	call affiche

boucle:
	sleep
    ; 	jump boucle
    JMP boucle


b1s:
	inc d0
    ; 	si d0 < 10 saut finb1s
    LDS R16,d0
    PUSH R16
    LDI R16,10
    POP R17
    CP R17,R16
    BRLO eti0
    CLR R16
    RJMP eti1
eti0:
    LDI R16,0x01
eti1:
    TST R16
    BREQ eti2
    JMP finb1s
eti2:

    ; 	d0 <- 0
    LDI R16,0
    STS d0,R16

    ; 	jump b10s
    JMP b10s

finb1s:
	call affiche
	reti

b10s:
	inc d1	
    ; 	si d1 < 10 saut finb10s
    LDS R16,d1
    PUSH R16
    LDI R16,10
    POP R17
    CP R17,R16
    BRLO eti3
    CLR R16
    RJMP eti4
eti3:
    LDI R16,0x01
eti4:
    TST R16
    BREQ eti5
    JMP finb10s
eti5:

    ; 	d0 <- 9
    LDI R16,9
    STS d0,R16

    ; 	d1 <- 9
    LDI R16,9
    STS d1,R16

finb10s:
	call affiche
	reti

affiche:
    ; 	PORTA@IO <- afficheur@ROM[d1]
    LDS R16,d1
    LDI R30,low(afficheur<<1)
    LDI R31,high(afficheur<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    OUT PORTA,R16

    ; 	PORTB@IO <- afficheur@ROM[d0]
    LDS R16,d0
    LDI R30,low(afficheur<<1)
    LDI R31,high(afficheur<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    OUT PORTB,R16

	ret

marche:
    ; 	route <- 1
    LDI R16,1
    STS route,R16

    ; 	PORTD@IO <- 0x80
    LDI R16,0x80
    OUT PORTD,R16

	reti

arret:
    ; 	route <- 0
    LDI R16,0
    STS route,R16

    ; 	d0 <- 0
    LDI R16,0
    STS d0,R16

    ; 	d1 <- 0
    LDI R16,0
    STS d1,R16

	call affiche
	reti

wd:
    ; 	si route = 0 saut finwd
    LDS R16,route
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti6
    CLR R16
    RJMP eti7
eti6:
    LDI R16,0x01
eti7:
    TST R16
    BREQ eti8
    JMP finwd
eti8:

	dec d0
    ; 	si d0 < 10 saut prefinwd
    LDS R16,d0
    PUSH R16
    LDI R16,10
    POP R17
    CP R17,R16
    BRLO eti9
    CLR R16
    RJMP eti10
eti9:
    LDI R16,0x01
eti10:
    TST R16
    BREQ eti11
    JMP prefinwd
eti11:

    ; 	d0 <- 9
    LDI R16,9
    STS d0,R16

	dec d1
    ; 	si d1 < 10 saut prefinwd
    LDS R16,d1
    PUSH R16
    LDI R16,10
    POP R17
    CP R17,R16
    BRLO eti12
    CLR R16
    RJMP eti13
eti12:
    LDI R16,0x01
eti13:
    TST R16
    BREQ eti14
    JMP prefinwd
eti14:

    ; 	route <- 0
    LDI R16,0
    STS route,R16

    ; 	PORTD@IO <- 0x00
    LDI R16,0x00
    OUT PORTD,R16

    ; 	d0 <- 0
    LDI R16,0
    STS d0,R16

    ; 	d1 <- 0
    LDI R16,0
    STS d1,R16

prefinwd:
	call affiche
finwd:
	reti
