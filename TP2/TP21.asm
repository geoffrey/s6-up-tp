.equ PINA = 0x00 ; d�finition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02
.equ PINB = 0x03
.equ DDRB = 0x04
.equ PORTB = 0x05

.equ EIMSK = 0x3D
.equ EICRA = 0x69
.equ EICRB = 0x6A
.equ SREG = 0x3F

.equ WDTCSR = 0x60

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D

.def chen = r19
.def port = r20

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x0002 ; D�finition du code � �xecuter lors des interruption
	jmp horaire

.org 0x0004 ; D�finition du code � �xecuter lors des interruption
	jmp antihoraire

.org 0x0080

debut:
    ;     DDRA@IO <- 0xFF ; Configuration des ports A et B en sortie
    LDI R16,0xFF
    OUT DDRA,R16

    ;     DDRB@IO <- 0xFF
    LDI R16,0xFF
    OUT DDRB,R16


    ;     EIMSK <- 0b00000011 ; On active les interruptions PD0 et PD1 sur front descendant
    LDI R16,0b00000011
    STS EIMSK,R16

    ;     EICRA <- 0b00001010
    LDI R16,0b00001010
    STS EICRA,R16

    ;     EICRB <- 0b00000000
    LDI R16,0b00000000
    STS EICRB,R16

    ; On active les interruptions au niveau du �P
    ;     SREG <-  0b10000000
    LDI R16,0b10000000
    STS SREG,R16


    ;     chen <- 0b00000001
    LDI R16,0b00000001
    STS chen,R16

    ;     chen <- 0xAA
    LDI R16,0xAA
    STS chen,R16

    ; 	port <- 0x00 ; pair : port A, impair : port B
    LDI R16,0x00
    STS port,R16

	call afficher
	sei

boucle:
	sleep ; On ne fait rien jusqu'� la prochaine interruption
    ; 	jump boucle
    JMP boucle


horaire:
    ; si (port & 0x01) == 0 alors lsl chen
    ;     si (port & 0x01) == 0 alors chen <- chen * 2
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti0
    CLR R16
    RJMP eti1
eti0:
    LDI R16,0x01
eti1:
    TST R16
    BREQ eti2
    LDS R16,chen
    LDI R17,2
    MUL R16,R17
    MOV R16,R0
    STS chen,R16
eti2:

    ; si (port & 0x01) == 1 alors lsr chen
    ; 	si (port & 0x01) == 1 alors chen <- chen / 2
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,1
    POP R17
    CP R17,R16
    BREQ eti3
    CLR R16
    RJMP eti4
eti3:
    LDI R16,0x01
eti4:
    TST R16
    BREQ eti6
    LDS R16,chen
    LDI R17,2
    SER R18
eti5:
    INC R18
    SUB R16,R17
    BRCC eti5
    MOV R16,R18
    STS chen,R16
eti6:

    ; 	si chen != 0 saut finhoraire
    LDS R16,chen
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BRNE eti7
    CLR R16
    RJMP eti8
eti7:
    LDI R16,0x01
eti8:
    TST R16
    BREQ eti9
    JMP finhoraire
eti9:


    ; 	si (port & 0x01) == 0 alors chen <- 0b10000000
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti10
    CLR R16
    RJMP eti11
eti10:
    LDI R16,0x01
eti11:
    TST R16
    BREQ eti12
    LDI R16,0b10000000
    STS chen,R16
eti12:

    ; 	si (port & 0x01) == 1 alors chen <- 0b00000001
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,1
    POP R17
    CP R17,R16
    BREQ eti13
    CLR R16
    RJMP eti14
eti13:
    LDI R16,0x01
eti14:
    TST R16
    BREQ eti15
    LDI R16,0b00000001
    STS chen,R16
eti15:

	inc port
finhoraire:
	call afficher
	reti

antihoraire:
    ; si (port & 0x01) == 0 alors lsr chen
    ;     si (port & 0x01) == 0 alors chen <- chen / 2
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti16
    CLR R16
    RJMP eti17
eti16:
    LDI R16,0x01
eti17:
    TST R16
    BREQ eti19
    LDS R16,chen
    LDI R17,2
    SER R18
eti18:
    INC R18
    SUB R16,R17
    BRCC eti18
    MOV R16,R18
    STS chen,R16
eti19:

    ; si (port & 0x01) == 1 alors lsl chen
    ; 	si (port & 0x01) == 1 alors chen <- chen * 2
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,1
    POP R17
    CP R17,R16
    BREQ eti20
    CLR R16
    RJMP eti21
eti20:
    LDI R16,0x01
eti21:
    TST R16
    BREQ eti22
    LDS R16,chen
    LDI R17,2
    MUL R16,R17
    MOV R16,R0
    STS chen,R16
eti22:

    ; 	si chen != 0 saut finantihoraire
    LDS R16,chen
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BRNE eti23
    CLR R16
    RJMP eti24
eti23:
    LDI R16,0x01
eti24:
    TST R16
    BREQ eti25
    JMP finantihoraire
eti25:


    ; 	si (port & 0x01) == 0 alors chen <- 0b00000001
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti26
    CLR R16
    RJMP eti27
eti26:
    LDI R16,0x01
eti27:
    TST R16
    BREQ eti28
    LDI R16,0b00000001
    STS chen,R16
eti28:

    ; 	si (port & 0x01) == 1 alors chen <- 0b10000000
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,1
    POP R17
    CP R17,R16
    BREQ eti29
    CLR R16
    RJMP eti30
eti29:
    LDI R16,0x01
eti30:
    TST R16
    BREQ eti31
    LDI R16,0b10000000
    STS chen,R16
eti31:

	inc port
finantihoraire:
	call afficher
	reti

afficher:
    ; 	si (port & 0x01) == 0 alors PORTB@IO <- 0x00
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti32
    CLR R16
    RJMP eti33
eti32:
    LDI R16,0x01
eti33:
    TST R16
    BREQ eti34
    LDI R16,0x00
    OUT PORTB,R16
eti34:

    ; 	si (port & 0x01) == 1 alors PORTA@IO <- 0x00
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,1
    POP R17
    CP R17,R16
    BREQ eti35
    CLR R16
    RJMP eti36
eti35:
    LDI R16,0x01
eti36:
    TST R16
    BREQ eti37
    LDI R16,0x00
    OUT PORTA,R16
eti37:

    ; 	si (port & 0x01) == 0 alors PORTA@IO <- chen
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti38
    CLR R16
    RJMP eti39
eti38:
    LDI R16,0x01
eti39:
    TST R16
    BREQ eti40
    LDS R16,chen
    OUT PORTA,R16
eti40:

    ; 	si (port & 0x01) == 1 alors PORTB@IO <- chen
    LDS R16,port
    ANDI R16,0x01
    PUSH R16
    LDI R16,1
    POP R17
    CP R17,R16
    BREQ eti41
    CLR R16
    RJMP eti42
eti41:
    LDI R16,0x01
eti42:
    TST R16
    BREQ eti43
    LDS R16,chen
    OUT PORTB,R16
eti43:

	ret
