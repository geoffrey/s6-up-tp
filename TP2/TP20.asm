.equ PINA = 0x00 ; définition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02

.equ WDTCSR = 0x60

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x0080

debut:
    ;     DDRA@IO <- 0x01 ; Configuration du port pour mettre la LED en sortie et le bouton en entrée
    LDI R16,0x01
    OUT DDRA,R16


attend:
	; On reste là si le bouton n'est pas appuyé
    ;     si (PINA@IO & 0x02) == 0 saut attend
    IN R16,PINA
    ANDI R16,0x02
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti0
    CLR R16
    RJMP eti1
eti0:
    LDI R16,0x01
eti1:
    TST R16
    BREQ eti2
    JMP attend
eti2:


    ; Dès que le bouton est appuyé
    cli ; Activation du chien de garde en mode RESET
    ;     WDTCSR <- 0b00010000 ; Autorise la modification du chien de garde
    LDI R16,0b00010000
    STS WDTCSR,R16

    ;     WDTCSR <- 0b00001110 ; Reset le programme après 1 seconde
    LDI R16,0b00001110
    STS WDTCSR,R16

    sei

    ;     PORTA@IO <- 0x01 ; On allume la LED
    LDI R16,0x01
    OUT PORTA,R16


boucle:
	; On reste ici en attendant que le watchdog arrive à expiration si le bouton est relâché
    ;     si (PINA@IO & 0x02) = 0 saut boucle
    IN R16,PINA
    ANDI R16,0x02
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti3
    CLR R16
    RJMP eti4
eti3:
    LDI R16,0x01
eti4:
    TST R16
    BREQ eti5
    JMP boucle
eti5:

    wdr ; Sinon si le bouton est appuyé on bloque le watchdog à 1s
    jmp boucle


