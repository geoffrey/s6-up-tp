.equ PINA = 0x00
.equ DDRA = 0x01
.equ PORTA = 0x02
.equ PINC = 0x06
.equ DDRC = 0x07
.equ PORTC = 0x08

.equ WDTCSR = 0x60
.equ TCCR0A = 0x24
.equ TCCR0B = 0x25
.equ TIMSK0 = 0x6E
.equ TIFR0 = 0x35

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D

.def d2 = r19
.def d1 = r20
.def d0 = r21
.def select = r22

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x0018 ; Interruption du watchdog
	jmp wd

.org 0x002E ; Interruption du timer
	jmp tm

.org 0x0080

afficheur:
    .DB 0x7E, 0x0C, 0x37, 0x9F, 0x4D, 0xDB, 0xFB, 0x0E, 0xFF, 0xDF
    ;      0,    1,    2,    3,    4,    5,    6,    7,    8,    9

debut:
    ; 	d2 <- 0
    LDI R16,0
    STS d2,R16

    ; 	d1 <- 0
    LDI R16,0
    STS d1,R16

    ; 	d0 <- 0
    LDI R16,0
    STS d0,R16

    ; 	select <- 0b00100000
    LDI R16,0b00100000
    STS select,R16


	cli
	; Configuration des ports
    ; 	DDRA@IO <- 0xFF
    LDI R16,0xFF
    OUT DDRA,R16

    ; 	DDRC@IO <- 0xFF
    LDI R16,0xFF
    OUT DDRC,R16


	; Watchdog toutes les secondes
    ; 	WDTCSR <- 0x10
    LDI R16,0x10
    STS WDTCSR,R16

    ; 	WDTCSR <- 0b01000110
    LDI R16,0b01000110
    STS WDTCSR,R16


	; Timer toutes les 2 ms
    ; 	TCCR0A@IO <- 0x00
    LDI R16,0x00
    OUT TCCR0A,R16

    ; 	TCCR0B@IO <- 0x04
    LDI R16,0x04
    OUT TCCR0B,R16

    ; 	TIMSK0 <- 0x01
    LDI R16,0x01
    STS TIMSK0,R16

    ; 	TIFR0 <- 0x01
    LDI R16,0x01
    STS TIFR0,R16

	sei

boucle:
	sleep
    ; 	jump boucle
    JMP boucle


wd:
	inc d0
    ; 	si d0 < 10 saut affichage
    LDS R16,d0
    PUSH R16
    LDI R16,10
    POP R17
    CP R17,R16
    BRLO eti0
    CLR R16
    RJMP eti1
eti0:
    LDI R16,0x01
eti1:
    TST R16
    BREQ eti2
    JMP affichage
eti2:

    ; 	d0 <- 0
    LDI R16,0
    STS d0,R16

	inc d1
    ; 	si d1 < 10 saut affichage
    LDS R16,d1
    PUSH R16
    LDI R16,10
    POP R17
    CP R17,R16
    BRLO eti3
    CLR R16
    RJMP eti4
eti3:
    LDI R16,0x01
eti4:
    TST R16
    BREQ eti5
    JMP affichage
eti5:

    ; 	d1 <- 0
    LDI R16,0
    STS d1,R16

	inc d2
    ; 	si d2 < 10 saut affichage
    LDS R16,d2
    PUSH R16
    LDI R16,10
    POP R17
    CP R17,R16
    BRLO eti6
    CLR R16
    RJMP eti7
eti6:
    LDI R16,0x01
eti7:
    TST R16
    BREQ eti8
    JMP affichage
eti8:

    ; 	d2 <- 0
    LDI R16,0
    STS d2,R16

affichage:
    ; Affichage Simulateur
	; PORTC@IO <- afficheur@ROM[d0]
	; PORTB@IO <- afficheur@ROM[d1]
	; PORTA@IO <- afficheur@ROM[d2]
	reti

tm:
    ; 	PortC@IO <- select
    LDS R16,select
    OUT PortC,R16

    ; 	si select = 0b10000000 alors PortA@IO <- afficheur@ROM[d2]
    LDS R16,select
    PUSH R16
    LDI R16,0b10000000
    POP R17
    CP R17,R16
    BREQ eti9
    CLR R16
    RJMP eti10
eti9:
    LDI R16,0x01
eti10:
    TST R16
    BREQ eti11
    LDS R16,d2
    LDI R30,low(afficheur<<1)
    LDI R31,high(afficheur<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    OUT PortA,R16
eti11:

    ; 	si select = 0b01000000 alors PortA@IO <- afficheur@ROM[d1]
    LDS R16,select
    PUSH R16
    LDI R16,0b01000000
    POP R17
    CP R17,R16
    BREQ eti12
    CLR R16
    RJMP eti13
eti12:
    LDI R16,0x01
eti13:
    TST R16
    BREQ eti14
    LDS R16,d1
    LDI R30,low(afficheur<<1)
    LDI R31,high(afficheur<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    OUT PortA,R16
eti14:

    ; 	si select = 0b00100000 alors PortA@IO <- afficheur@ROM[d0]
    LDS R16,select
    PUSH R16
    LDI R16,0b00100000
    POP R17
    CP R17,R16
    BREQ eti15
    CLR R16
    RJMP eti16
eti15:
    LDI R16,0x01
eti16:
    TST R16
    BREQ eti17
    LDS R16,d0
    LDI R30,low(afficheur<<1)
    LDI R31,high(afficheur<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    OUT PortA,R16
eti17:

	lsl select
    ; 	si select = 0 alors select <- 0b00100000
    LDS R16,select
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti18
    CLR R16
    RJMP eti19
eti18:
    LDI R16,0x01
eti19:
    TST R16
    BREQ eti20
    LDI R16,0b00100000
    STS select,R16
eti20:

	reti

