.equ PINA = 0x00 ; définition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x0080

debut:
    ldi r16,0x01 ; Configuration de la direction des ports
    ; PA0 : Sortie (LED) donc 1
    ; PA1 : Entrée (Bouton) donc 0
    ; PA7-2 : Indéfférent donc arbitrairement 0
    out DDRA,r16

boucle:
    in r16,PINA ; Lecture du port A
    lsr r16 ; Décalage vers la droite : le bit associé au bouton va sur celui associé à la LED
    ; ldi r16,0x01 ; Test pour vérifier la LED
    out PORTA,r16 ; Écriture du port A
    jmp boucle ; On recommence

