.equ PINA = 0x00 ; définition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02
.equ PINC = 0x06
.equ DDRC = 0x07
.equ PORTC = 0x08

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D

.def numero = r20
.def io = r21
.def test = r19

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x0080

afficheur:
    .DB 0x7E, 0x0C, 0x37, 0x9F, 0x4D, 0xDB, 0xFB, 0x0E, 0xFF, 0xDF
    ;      0,    1,    2,    3,    4,    5,    6,    7,    8,    9

touche:
    .DB 0x41, 0x88, 0x48, 0x28, 0x84, 0x44, 0x24, 0x82, 0x42, 0x22
    ;      0,    1,    2,    3,    4,    5,    6,    7,    8,    9


debut:
    ldi r16,0xFF ; Port A : 7 segments
    out DDRA,r16

    ldi r16,0x0F ; Port C : Clavier
    out DDRC,r16
    
    numero <- 0

boucle:
	; On lit le bouton
	test <- 0
boucleTest:
	io <- touche@ROM[test]
	out portc,io
	in io,pinc
	si io = touche@ROM[test] alors numero <- test
	inc test	
	si test < 10 saut boucleTest
	
    ; On affiche compteur sur le 7 segments
    io <- afficheur@ROM[numero]
    out porta,io

    ; On continue    
    saut boucle

tempo:
    ldi r24,8
tempoA:
    subi r22,1
    sbci r23,0
    sbci r24,0
    brcc tempoA
    ret
