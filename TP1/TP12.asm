.equ PINA = 0x00 ; définition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02
.equ PINC = 0x06
.equ DDRC = 0x07
.equ PORTC = 0x08

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D

.def compteur = r20
.def seg = r21

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x0080

afficheur:
    .DB 0x7E, 0x0C, 0x37, 0x9F, 0x4D, 0xDB, 0xFB, 0x0E, 0xFF, 0xDF
	;.DB 0x7E, 0x30, 0x6D, 0x79, 0x33, 0x5B, 0x5F, 0x70, 0x7F, 0x7B
    ;      0,    1,    2,    3,    4,    5,    6,    7,    8,    9


debut:
    ldi r16,0xFF ; config direction ports
    out DDRA,r16
    out DDRC,r16

    ldi r16,0x40 ; On sélectionne l'afficheur du millieu
    out PORTC,r16
    ;     compteur <- 0
    LDI R16,0
    STS compteur,R16



boucle:

    ; On affiche compteur sur le 7 segments
    ;     seg <- afficheur@ROM[compteur]
    LDS R16,compteur
    LDI R30,low(afficheur<<1)
    LDI R31,high(afficheur<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    STS seg,R16

    out porta,seg

    ; On incrémente compteur
    inc compteur
    ;     si compteur > 9 alors compteur <- 0
    LDS R16,compteur
    PUSH R16
    LDI R16,9
    POP R17
    CP R17,R16
    BREQ eti0
    BRLO eti0
    LDI R16,0x01
    RJMP eti1
eti0:
    CLR R16
eti1:
    TST R16
    BREQ eti2
    LDI R16,0
    STS compteur,R16
eti2:


    ; On attend et on recommence
    call tempo
    ;     saut boucle
    JMP boucle


tempo:
    ldi r24,8
tempoA:
    subi r22,1
    sbci r23,0
    sbci r24,0
    brcc tempoA
    ret

