.equ PINA = 0x00 ; définition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02
.equ PINB = 0x03
.equ DDRB = 0x04
.equ PORTB = 0x05

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x0080

debut:
    ldi r16,0xFF ; config direction ports
    out DDRA,r16
    out DDRB,r16
    ;     r17 <- 0x01 ; 8 places de chenillard
    LDI R16,0x01
    MOV R17,R16



boucleA:
	; On affiche l'état courant
	out porta,r17

	call tempo

    ; On calcule l'état suivant

    lsl r17
    ;     si r17 > 0 saut boucleA
    MOV R16,R17
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti0
    BRLO eti0
    LDI R16,0x01
    RJMP eti1
eti0:
    CLR R16
eti1:
    TST R16
    BREQ eti2
    JMP boucleA
eti2:


    out porta,r17
    ;     r17 <- 0b10000000
    LDI R16,0b10000000
    MOV R17,R16

    ;     saut boucleB
    JMP boucleB


boucleB:
	; On affiche l'état courant
	out portb,r17

	call tempo;

    ; On calcule l'état suivant

    lsr r17
    ;     si r17 > 0 saut boucleB
    MOV R16,R17
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti3
    BRLO eti3
    LDI R16,0x01
    RJMP eti4
eti3:
    CLR R16
eti4:
    TST R16
    BREQ eti5
    JMP boucleB
eti5:


    out portb,r17
    ;     r17 <- 0x01
    LDI R16,0x01
    MOV R17,R16

    ;     saut boucleA
    JMP boucleA


tempo:
	; On fait une pause
    ldi r24,8
tempoA:
    subi r22,1
    sbci r23,0
    sbci r24,0
    brcc tempoA
    ret
