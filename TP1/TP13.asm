.equ PINA = 0x00 ; définition des adresses des ports
.equ DDRA = 0x01
.equ PORTA = 0x02
.equ PINC = 0x06
.equ DDRC = 0x07
.equ PORTC = 0x08

.equ RAMEND = 0x21FF
.equ SPH = 0x3E ; initialisation de la pile
.equ SPL = 0x3D

.def numero = r20
.def io = r21
.def test = r19

.org 0x000
    ; Vecteur RESET
    jmp debut

.org 0x0080

afficheur:
    .DB 0x7E, 0x0C, 0x37, 0x9F, 0x4D, 0xDB, 0xFB, 0x0E, 0xFF, 0xDF
    ;      0,    1,    2,    3,    4,    5,    6,    7,    8,    9

touche:
    .DB 0x41, 0x88, 0x48, 0x28, 0x84, 0x44, 0x24, 0x82, 0x42, 0x22
    ;      0,    1,    2,    3,    4,    5,    6,    7,    8,    9


debut:
    ldi r16,0xFF ; Port A : 7 segments
    out DDRA,r16

    ldi r16,0x0F ; Port C : Clavier
    out DDRC,r16

    ;     numero <- 0
    LDI R16,0
    STS numero,R16


boucle:
	; On lit le bouton
    ; 	test <- 0
    LDI R16,0
    STS test,R16

boucleTest:
    ; 	io <- touche@ROM[test]
    LDS R16,test
    LDI R30,low(touche<<1)
    LDI R31,high(touche<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    STS io,R16

	out portc,io
	in io,pinc
    ; 	si io = touche@ROM[test] alors numero <- test
    LDS R16,io
    PUSH R16
    LDS R16,test
    LDI R30,low(touche<<1)
    LDI R31,high(touche<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    POP R17
    CP R17,R16
    BREQ eti0
    CLR R16
    RJMP eti1
eti0:
    LDI R16,0x01
eti1:
    TST R16
    BREQ eti2
    LDS R16,test
    STS numero,R16
eti2:

	inc test	
    ; 	si test < 10 saut boucleTest
    LDS R16,test
    PUSH R16
    LDI R16,10
    POP R17
    CP R17,R16
    BRLO eti3
    CLR R16
    RJMP eti4
eti3:
    LDI R16,0x01
eti4:
    TST R16
    BREQ eti5
    JMP boucleTest
eti5:


    ; On affiche compteur sur le 7 segments
    ;     io <- afficheur@ROM[numero]
    LDS R16,numero
    LDI R30,low(afficheur<<1)
    LDI R31,high(afficheur<<1)
    CLR R17
    ADD R30,R16
    ADC R31,R17
    LPM R16,Z
    STS io,R16

    out porta,io

    ; On continue    
    ;     saut boucle
    JMP boucle


tempo:
    ldi r24,8
tempoA:
    subi r22,1
    sbci r23,0
    sbci r24,0
    brcc tempoA
    ret
