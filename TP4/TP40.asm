; lecture de la liaison série

.equ RAMEND = 0x21FF
.equ SPH = 0x3E
.equ SPL = 0x3D

.equ UBRR0H = 0x00C5
.equ UBRR0L = 0x00C4
.equ UCSR0A = 0x00C0
.equ UCSR0B = 0x00C1
.equ UCSR0C = 0x00C2
.equ UDR0   = 0x00C6

.org 0x0000
    ; Vecteur RESET
    jmp debut

.org 0x0080
debut:
    ; initialisation du pointeur de pile
    ldi r28,low(RAMEND)
    ldi r29,high(RAMEND)
    out SPL, r28
    out SPH, r29

    ;     UBRR0H <- 3
    LDI R16,3
    STS UBRR0H,R16

    ;     UBRR0L <- 64
    LDI R16,64
    STS UBRR0L,R16

    ;     UCSR0A <- 0b00000110
    LDI R16,0b00000110
    STS UCSR0A,R16

    ;     UCSR0B <- 0b00011000
    LDI R16,0b00011000
    STS UCSR0B,R16

    ;     UCSR0C <- 0b00000110
    LDI R16,0b00000110
    STS UCSR0C,R16


boucle:
    ; attend qu'un caractère arrive
    ;     si (UCSR0a & 0x80) == 0 saut boucle
    LDS R16,UCSR0a
    ANDI R16,0x80
    PUSH R16
    LDI R16,0
    POP R17
    CP R17,R16
    BREQ eti0
    CLR R16
    RJMP eti1
eti0:
    LDI R16,0x01
eti1:
    TST R16
    BREQ eti2
    JMP boucle
eti2:


    ;     R20 <- UDR0
    LDS R16,UDR0
    MOV R20,R16


    ;     si R20 == 'a' alors R20 <- 'A'
    MOV R16,R20
    PUSH R16
    LDI R16,'a'
    POP R17
    CP R17,R16
    BREQ eti3
    CLR R16
    RJMP eti4
eti3:
    LDI R16,0x01
eti4:
    TST R16
    BREQ eti5
    LDI R16,'A'
    MOV R20,R16
eti5:

    ;     si R20 == 'e' alors R20 <- 'E'
    MOV R16,R20
    PUSH R16
    LDI R16,'e'
    POP R17
    CP R17,R16
    BREQ eti6
    CLR R16
    RJMP eti7
eti6:
    LDI R16,0x01
eti7:
    TST R16
    BREQ eti8
    LDI R16,'E'
    MOV R20,R16
eti8:


    ;     UDR0 <- R20
    STS UDR0,R20


    ;     saut boucle
    JMP boucle



